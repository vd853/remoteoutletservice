﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RemoteOutletClient.ListenerService;

namespace RemoteOutletClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Schema A_B A = outlet number, B = control
        // 1=on, 2=off, 3=5min, 0=reset
        public MainWindow()
        {
            Init.Start();
            InitializeComponent(); 
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var s = new ListenerClient())
                {
                    var c = new ListenerService.Control();
                    c.Mode = Mode.Test;
                    Response.Content = s.SetMode(c);
                }
            }
            catch
            {
                
            }
            Response.Content = "Initial test was sent";
        }

        private void On(int outlet)
        {
            using (var s = new ListenerClient())
            {
                var c = new ListenerService.Control();
                c.Outlet = outlet;
                c.Mode = Mode.On;
                Response.Content = s.SetMode(c);
            }
        }
        private void Off5min(int outlet)
        {
            using (var s = new ListenerClient())
            {
                var c = new ListenerService.Control();
                c.Outlet = outlet;
                c.Mode = Mode.Off5;
                Response.Content = s.SetMode(c);
            }
        }
        private void Off(int outlet)
        {
            using (var s = new ListenerClient())
            {
                var c = new ListenerService.Control();
                c.Outlet = outlet;
                c.Mode = Mode.Off;
                Response.Content = s.SetMode(c);
            }
        }
        private void Reset(int outlet)
        {
            using (var s = new ListenerClient())
            {
                var c = new ListenerService.Control();
                c.Outlet = outlet;
                c.Mode = Mode.Reset;
                Response.Content = s.SetMode(c);
            }
        }

        #region Events

        private void On01_Click(object sender, RoutedEventArgs e)
        {
            On(0);
        }

        private void Off503_Click(object sender, RoutedEventArgs e)
        {
            Off5min(0);
        }

        private void Reset00_Click(object sender, RoutedEventArgs e)
        {
            Reset(0);
        }

        private void Off02_Click(object sender, RoutedEventArgs e)
        {
            Off(0);
        }

        private void On11_Click(object sender, RoutedEventArgs e)
        {
            On(1);
        }

        private void Off513_Click(object sender, RoutedEventArgs e)
        {
            Off5min(1);
        }

        private void Reset10_Click(object sender, RoutedEventArgs e)
        {
            Reset(1);
        }

        private void Off12_Click(object sender, RoutedEventArgs e)
        {
            Off(1);
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.MessageBox.Show("Change endpoint address from: RemoteOutletClient.exe.config");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        
    }
}
