﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RemoteOutletLibrary;
using Topshelf;
using Topshelf.Builders;
using Utilities;

namespace RemoteOutletHost
{ 
    class Program
    {
        public static string port = "";
        private static Service serve;
        static void Main()
        {
            HostFactory.Run(x =>                                 //1
            {
                x.Service<Service>(s =>                        //2
                {
                    s.ConstructUsing(()=> new Service());
                    s.WhenStarted(a => a.Start());              //4
                    s.WhenStopped(a => a.Stop());      
                });
                x.RunAsLocalSystem();                            //6

                x.SetDescription("Controls Arduino Outlet");        //7
                x.SetDisplayName("ArudinoOutlet");                       //8
                x.SetServiceName("ArudinoOutlet");                       //9
            });                                                  //10
        }
    }

    class Service
    {
        public static bool PortDefined = false;
        public static ServiceHost host;
        public Service()
        {           
            host = new ServiceHost(typeof(Listen));
        }
        public void Start()
        {
            if (Listen.l == null)
            {
                Listen.l = new FileLogL4N(FileLogL4N.GenerateFileName(), 2);
                Listen.l.L.Info("Logger singleton created");
            }
            GetPort();
            Listen.Write("NULL"); //send first message
            Listen.l.L.Info("Host have started");
            foreach (var descriptionEndpoint in host.Description.Endpoints)
            {
                Listen.l.L.Info(descriptionEndpoint.Name);
                Listen.l.L.Info(descriptionEndpoint.Address);
            }
            host.Open();
        }

        public void Stop()
        {
            host.Close();
        }

        public static void uStop()
        {
            host.Close();
        }

        private void GetPort()
        {
            if (!PortDefined)
            {
                var PortPath = System.IO.Directory.GetCurrentDirectory() + @"\Port.txt";
                try
                {
                    Program.port = System.IO.File.ReadAllText(PortPath);
                    Listen.l.L.Info("Using port: " + Program.port);
                }
                catch
                {
                    Listen.l.L.Info("Cannot open Port.txt");
                }
                PortDefined = true;
            }
        }
    }
}
