﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RemoteOutletHost;
using Utilities;


namespace RemoteOutletLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Listener" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Listen : IListener
    {
        private const int NumberOutlet = 2;
        private const int resetDelay = 5000;
        private const int WaitOff = 300000; //5min in ms

        //Singletons
        public static FileLogL4N l;

        private static CancellationTokenSource[] OutletCTS = new CancellationTokenSource[NumberOutlet];
        private static Task[] Off5 = new Task[NumberOutlet];
        private int outlet;
        private mode mode;

        // Schema A_B A = outlet number, B = control
        // 1=on, 2=off, 3=5min, 0=reset
        public string SetMode(Control setter)
        {
            init(setter);
            var success = false;
            switch (mode)
            {
                case mode.On:
                    cancel();
                    success = On(setter);
                    break;
                case mode.Test:
                    cancel();
                    Test();
                    success = true;
                    break;
                case mode.Off:
                    cancel();
                    success = Off(setter);
                    break;
                case mode.Reset:
                    cancel();
                    log("Reset triggered");
                    var s = setter;
                    Task.Run(() =>
                    {
                        Off(s);
                        Thread.Sleep(resetDelay);
                        On(s);
                    });
                    success = true;
                    break;
                case mode.Off5:
                    // cancels previous token
                    if (Off5[outlet] != null)
                    {
                        if (!Off5[outlet].IsCompleted)
                        {
                            cancel();
                            log("Previous Off5 has just been cancel for new one");
                        }
                    }
                    log("Off5 has just been triggered");

                    // create new TS and timer
                    OutletCTS[outlet] = new CancellationTokenSource();
                    var token = OutletCTS[outlet].Token;
                    Off5[outlet] = Task.Run(() =>
                    {
                        Task.Delay(WaitOff).Wait();
                        if (token.IsCancellationRequested)
                        {
                            log("Previous trigger cancellation was invoked");
                        }
                        else
                        {
                            Off(setter);
                            log("Trigger off from delayed off of time: " + WaitOff);
                        }
                    }, token);
                    success = true;
                    break;
            }
            if (success)
            {
                return mode + " trigger was successful";
            }
            return mode + " trigger FAILED";
        }

        private void init(Control setter)
        {
            mode = setter.Mode;
            outlet = setter.Outlet;

            if (OutletCTS[outlet] == null)
            {
                OutletCTS[outlet] = new CancellationTokenSource();
                l.L.Info("New TS created");
            }
        }
        private bool Test()
        {
            try
            {
                Write("Test"); //Does nothing on the microcontroller
                log("Test triggered");
                return true;
            }
            catch
            {
                log("Test triggered FAILED");
                return false;
            }
        }
        private bool On(Control c)
        {
            try
            {
                Write(c.Outlet + "_1");
                log("On triggered");
                return true;
            }
            catch
            {
                log("On triggered FAILED");
                return false;
            }
        }

        private bool Off(Control c)
        {
            try
            {
                Write(c.Outlet + "_2");
                log("Off triggered");
                return true;
            }
            catch
            {
                log("Off triggered FAILED");
                return false;
            }
        }

        public static void Write(string command)
        {
            using (var Arduino = new SerialPort("COM" + Program.port))
            {
                Arduino.Open();
                Arduino.Write(command);
                Arduino.Close();
            }
        }
        void cancel()
        {
            if (!OutletCTS[outlet].IsCancellationRequested)
            {
                OutletCTS[outlet].Cancel();
            }

        }
        private void log(string message)
        {
            var m = DateTime.Now + @" \\ " + outlet + " : " + message;
            l.L.Info(m);
        }
    }
}
