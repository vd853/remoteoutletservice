﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RemoteOutletLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IListeners" in both code and config file together.
    [ServiceContract]
    public interface IListener
    {
        [OperationContract]
        string SetMode(Control setter);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "RemoteOutletLibrary.ContractType".
    [DataContract]
    public class Control
    {
        [DataMember]
        public mode Mode { get; set; }

        [DataMember]
        public int Outlet { get; set; }
    }

    [DataContract(Name = "Mode")]
    public enum mode
    {
        [EnumMember]
        On,
        [EnumMember]
        Off,
        [EnumMember]
        Reset,
        [EnumMember]
        Off5,
        [EnumMember]
        Test
    }
}
