﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Trace.WriteLine(DateTime.Now + " Start");
            var t1 = Task.Run(() =>
            {
                Task.Delay(3000).Wait();
                Trace.WriteLine(DateTime.Now + " COMPLETED task delay");
            });
            Task.Run(() =>
            {
                Thread.Sleep(1000);
                Trace.WriteLine(DateTime.Now + " COMPLETED thread sleep");
            });
            Thread.Sleep(4000);
            Trace.WriteLine(DateTime.Now + " Completed");
        }
    }
}
