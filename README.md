# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

I created this project for the purpose of being able to control multiple outlet source using a desktop base application and web-based application. The implementation includes both software and electronic hardware. It was built using an Arduino microcontroller and various power modules. The application runs on .Net framework, particularly connecting the COM interface with WCF. The WCF side is connected as a web backend and frontend through ASP.NET MVC, and as a desktop application using WPF. 

![Scheme](/Diagram.jpg)
![Scheme](/1.jpg)

### How do I get set up? ###

I did not intend to make this for public use but it goes something like this…

-Build the electronic parts and upload the Arduino script to any Arduino compatible microcontroller. 

-On the application client, you must edit the endpoint from the RemoteOutletClient.exe config file to match the endpoint IP on the host.

-On the host, you must edit the RemoteOutletHost.exe config file to match the server’s IP on the endpoint, and then you have to follow the instruction from Topshelf to install the service, which is a basic install command line. 

-In your ASP.NET MVC application, you must add a service reference from the host, which should already be running. This reference should be accessible to your Controller, and you will need to build a web page to access the functions. I just use Razor to accomplish this.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin